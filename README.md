A child image of https://hub.docker.com/r/lachlanevenson/k8s-kubectl. Sets the `ENTRYPOINT` to `[]` for use in gitlab-ci.
